﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    GameController gameController;

    public List<GameObject> npcList;
    public Vector3 spawnValues;
    public int hazardCount;

    void Awake()
    {
        gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

        if (npcList == null || npcList.Count == 0)
            return;
        
        for (int i = 0; i < hazardCount; i++)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            GameObject npc = npcList[(int) Random.Range(0, npcList.Count)];
            Instantiate(npc, spawnPosition, spawnRotation);
            gameController.AddNPC();
        }
    }

}
