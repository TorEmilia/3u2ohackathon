﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TappingNextScene : MonoBehaviour {

    public string NazwaSceny;
	// Use this for initialization
	void Start () {
        Handheld.Vibrate();
	}
	
	// Update is called once per frame
	void Update () {
        Handheld.Vibrate();
        if (Input.touchCount > 0)
        SceneManager.LoadScene(NazwaSceny);
        
    }
}
