﻿using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour {
    private GameController gameController;

    public Slider timerSlider;
    public int startingTime;
    private float currentTime;

	// Use this for initialization
    void Start () {
        gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        currentTime = (float) startingTime;
	}
	
	// Update is called once per frame
	void Update () {
        currentTime -= Time.deltaTime;
        timerSlider.value = currentTime / startingTime * timerSlider.maxValue;

        if(currentTime <= 0) {
            gameController.GameOver();
        }
	}
}
