﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitController : MonoBehaviour {

    private GameController gameController;
    private Animator[] animators; 

    public void Start()
	{
        gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        animators = GetComponentsInChildren<Animator>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
        if(collision.gameObject.tag == "NPC") {
            gameController.OnNPCDestroyed();
            Destroy(collision.gameObject);

            foreach(Animator animator in animators)
            {
                animator.SetTrigger("OpenDoor");
            }
        }
	}
}
