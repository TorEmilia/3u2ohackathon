﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextSliding : MonoBehaviour
{

    public Text text;
    // Speed in units per sec.
    public float speed;

    void FixedUpdate()
    {
        text.transform.position = Vector3.MoveTowards(text.transform.position, new Vector3(450, 110), speed * Time.deltaTime * 4);
    }
}

