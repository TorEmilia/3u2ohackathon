﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour {
    public AudioSource Clicks;
    private AudioClip[] ClickSound = new AudioClip[3];

    // Use this for initialization
    void Start () {
        ClickSound = Resources.LoadAll<AudioClip>("Clicks");

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            Clicks.clip = ClickSound[Random.Range(0, ClickSound.Length)];
            Clicks.Play();
        }

    }
}
