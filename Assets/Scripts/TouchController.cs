﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TouchController : MonoBehaviour
{
    public GameObject pushObject;


    private Vector3 mousePosition;
    GameController gameController;
    public float dragSpeed = 2;
    public Camera camera;
    private Vector3 dragOrigin;

    public AudioSource Clicks;
    AudioClip[] ClickSound = new AudioClip[3];

    bool moved = false;
    
    // Use this for initialization
    void Awake()
    {
        ClickSound = Resources.LoadAll<AudioClip>("Clicks");
        gameController = GetComponent<GameController>();
    }
    // Update is called once per frame
    void Update()
    {
        moved = false;


        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                moved = false;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                moved = true;
                moveScreen(touch);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (!moved)
                {
                    explodeNpcs(touch.position);
                    Clicks.clip = ClickSound[Random.Range(0, ClickSound.Length)];
                    Clicks.Play();
                }
            }
        }
    }

    private void moveScreen(Touch touch) {
        Vector3 temp = new Vector3(touch.deltaPosition.x, touch.deltaPosition.y);
        Vector3 pos = Camera.main.ScreenToViewportPoint(temp - dragOrigin);
        Vector3 move = new Vector3(-pos.x * dragSpeed, -pos.y * dragSpeed, 0);
        if ((camera.transform.position.y + move.y < -4 || camera.transform.position.y + move.y > 4) && !(camera.transform.position.x + move.x < -5 || camera.transform.position.x + move.x > 6))
        {
            move.y = 0;
        }
        else if (camera.transform.position.y + move.y < -4 || camera.transform.position.y + move.y > 4) return;
        if ((camera.transform.position.x + move.x < -5 || camera.transform.position.x + move.x > 6) && !(camera.transform.position.y + move.y < -4 || camera.transform.position.y + move.y > 4))
        {
            move.x = 0;
        }
        if ((camera.transform.position.x + move.x < -5 || camera.transform.position.x + move.x > 6)) return;
        camera.transform.Translate(move, Space.World);
        moved = false;
    }

    private void explodeNpcs(Vector3 newPosition) {
        if (gameController.OnStaminaTouch() == false)
            return;

        moved = true;
        Debug.Log("Bum!");
        Debug.Log("x!" + newPosition.x);
        Debug.Log("y !" + newPosition.y);

        Vector3 realWorldPos = Camera.main.ScreenToWorldPoint(newPosition);
        Instantiate(pushObject, realWorldPos, Quaternion.identity);

    }
}