﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DoorGrunts : MonoBehaviour {

    public AudioSource Grunts;
    public AudioSource Point;
    private AudioClip[] GruntsAudio = new AudioClip[7];

    // Use this for initialization
    void Start () {
        GruntsAudio=Resources.LoadAll<AudioClip>("Sounds");
    }
	
	// Update is called once per frame
	void Update () {
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NPC")
        {
            Grunts.clip = GruntsAudio[Random.Range(0, GruntsAudio.Length)];
            Grunts.Play();
            Point.Play();
        }
    }
}
