﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeIn : MonoBehaviour {

    public GameObject obj;
    private bool canFade;
    private Color alphaColor;
    public float timeToStart = 2f;
    private float timeToFade = 1.0f;

    public void Start()
    {
        canFade = false;
        alphaColor = obj.GetComponent<Renderer>().material.color;
        alphaColor.a = 0;
    }
    public void Update()
    {
        if (canFade)
        {
            obj.GetComponent<MeshRenderer>().material.color = Color.Lerp(obj.GetComponent<Renderer>().material.color, alphaColor, timeToFade * Time.deltaTime);
        }
    }
}
