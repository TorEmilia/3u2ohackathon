﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScreenController : MonoBehaviour {

    float timeToNext = 2f;
    public GameObject button;

    // Use this for initialization
    void Start()
    {
        button.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        timeToNext -= Time.deltaTime;

        if (timeToNext <= 0)
        {
            button.SetActive(true);
            if (Input.touchCount > 0)
                SceneManager.LoadScene("RingRing");
        }
    }
}
