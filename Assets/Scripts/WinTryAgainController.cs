using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinTryAgainController : MonoBehaviour {

    float timeToNext = 2f;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        timeToNext -= Time.deltaTime;

        if (timeToNext <= 0)
        {
            if (Input.touchCount > 0)
                SceneManager.LoadScene("RingRing");
        }
    }
}
