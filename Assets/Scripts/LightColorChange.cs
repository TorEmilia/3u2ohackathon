﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightColorChange : MonoBehaviour {

    public Color KolorStartowy;
    SpriteRenderer LightRenderer;
    public float deltaH = 0.05f; 
    float h, s, v;
    
	// Use this for initialization
	void Start () {
        LightRenderer = GetComponent<SpriteRenderer>();
        KolorStartowy = LightRenderer.color;
    }

    // Update is called once per frame
    void Update () {
        //LightRenderer.material.color = new Color32(78, 0, 224, 205);
            Color.RGBToHSV(LightRenderer.color, out h, out s, out v);

            h += deltaH;
            if (h > 1) h = 0;
            Color ModifiedColor = Color.HSVToRGB(h, s, v);
            LightRenderer.color = new Color(ModifiedColor.r, ModifiedColor.g, ModifiedColor.b,KolorStartowy.a);
            
    }
}
