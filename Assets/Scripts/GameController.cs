﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    TimerController timerController;
    StaminaController staminaController;
    public Text textPoints;

    int NPCCount;

	private void Awake()
    {
        timerController = GetComponent<TimerController>();
        staminaController = GetComponent<StaminaController>();
        NPCCount = 0;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        textPoints.text = NPCCount.ToString();
    }

    public bool OnStaminaTouch()
    {
        return staminaController.OnTouch();
    }

    public void AddNPC() {
        NPCCount++;
    }

    public void OnNPCDestroyed() {
        NPCCount--;
        if(NPCCount == 0) {
            GameWon();
        }
    }

    public void GameWon()
    {
        SceneManager.LoadScene("WinScene");
    }

    public void GameOver() {
        SceneManager.LoadScene("GameOverScene");
    }
}
