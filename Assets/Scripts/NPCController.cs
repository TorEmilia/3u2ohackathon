﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour {

    private Animator animator;
    private Rigidbody2D rigidbody2d;
    Vector3 oldPosition;

    public float randomMovesScale;

    float timeToChangeDirection;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        rigidbody2d = GetComponent<Rigidbody2D>();
        oldPosition = transform.position;
        timeToChangeDirection = Random.Range(2, 5);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timeToChangeDirection -= Time.deltaTime;
        if (timeToChangeDirection <= 0) {
            timeToChangeDirection = Random.Range(2, 5);
            MoveInRandomDirection();
        }

        Vector3 moveDirection = transform.position - oldPosition;
        if (moveDirection != Vector3.zero) {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg + 90;

            Vector3 temp = transform.rotation.eulerAngles;
            temp.z = angle;
            transform.rotation = Quaternion.Euler(temp);
        }
        oldPosition = transform.position;

        if(rigidbody2d.velocity.magnitude < 0.1f) {
            animator.SetInteger("state", 0);
        } else {
            animator.SetInteger("state", 1);
        }
	}

    private void MoveInRandomDirection()
    {
        float angle = Random.Range(0f, 360f);
        Quaternion quat = Quaternion.AngleAxis(angle, Vector3.forward);
        Vector3 newUp = quat * Vector3.up;
        newUp.z = 0;
        newUp.Normalize();
        transform.up = newUp;

        Vector2 forceVector = new Vector2(Random.Range(-1, 1), Random.Range(-1, 1));
        forceVector.Normalize();
        forceVector.x *= randomMovesScale;
        forceVector.y *= randomMovesScale;
        rigidbody2d.AddForce(forceVector);
    }
}
