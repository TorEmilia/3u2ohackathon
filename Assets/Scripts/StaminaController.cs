using UnityEngine;
using UnityEngine.UI;

public class StaminaController : MonoBehaviour
{

    public Slider staminaSlider;
    public float maxStamina;
    public float staminaRegenRate;
    public float actionCost;

    private float currentStamina;

	// Use this for initialization
	void Start()
	{
        currentStamina = maxStamina;
	}

	// Update is called once per frame
	void Update()
    {
        currentStamina = Mathf.Min(currentStamina + staminaRegenRate*Time.deltaTime, maxStamina);
        staminaSlider.value = currentStamina / maxStamina * staminaSlider.maxValue;
	}

    public bool OnTouch() {
        if(currentStamina > actionCost) {
            currentStamina -= actionCost;
            return true;
        } else {
            return false;
        }
    }
}
